/* ---------------------------------------------------------------------------
	Standard CRC calculation source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"

#define WIDTH  (8*sizeof(uint8_t))
#define TOPBIT (1<<(WIDTH-1))


uint8_t std_crc_calc(uint8_t message[], int nBytes, uint8_t polynomial){
    uint8_t  remainder=0;	

// Perform modulo -2 division, a byte at a time
    for (int byte=0;byte<nBytes;++byte){
    
// Bring the next byte into the remainder.
        remainder^=(message[byte]<<(WIDTH - 8));

//Perform modulo-2 division, a bit at a time.
        for (uint8_t bit=8;bit>0;--bit){
        
//Try to divide the current data bit.
            if (remainder&TOPBIT){
                remainder=(remainder<<1)^polynomial;
            }
            else{
                remainder=(remainder<<1);
            }
        }
    }
//The final remainder is the CRC result.
    return (remainder);
}   /* crcSlow() */
