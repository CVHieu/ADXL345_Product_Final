/* ---------------------------------------------------------------------------
	External interrupt header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#ifndef _EINT_H_
#define _EINT_H_

extern volatile uint8_t flag_en_pir,en_pir;

void eint_init(void);

#endif