/* ---------------------------------------------------------------------------
	Source file for MMA7455
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"

void init_MMA7455(void){

	spi_init(SPI_MODE_3,SPI_MSB,SPI_NO_INTERRUPT,SPI_MSTR_CLK32);

	MMA7455_write_byte(_POWER_CTL,MMA7455_STANDBY);
	delay_ms(50);

	MMA7455_write_byte(_DATA_FORMAT,MMA7455_2G);
	delay_ms(50);

	MMA7455_write_byte(_BW_RATE,_SPEED);
	delay_ms(50);

	MMA7455_write_byte(_POWER_CTL,MMA7455_MEASURE);
	delay_ms(50);
}

void MMA7455_write_byte(uint8_t add,uint8_t data){
	select_mma();
	send_spi(add|WRITE);
	send_spi(data);
	deselect_mma();
}

uint8_t MMA7455_read_byte(uint8_t add){
uint8_t data;
	select_mma();
	send_spi(add|READ);
	data=send_spi(0xFF);
	deselect_mma();
	return data;
}


void read_MMA7455(uint16_t *x, uint16_t *y, uint16_t *z){
// 10bit
// 0x0200 -> 0x03FF so am
// 0x0000 -> 0x01FF so duong
uint16_t temp=0;
uint8_t buff=0;
	buff=MMA7455_read_byte(MMA7455_X_OUTL);
	temp|=(uint16_t)MMA7455_read_byte(MMA7455_X_OUTH);
	temp=temp<<8;
	temp|=buff;
	*x=(temp^0x1000)&0x1FFF;
	
	temp=0;buff=0;
	buff=MMA7455_read_byte(MMA7455_Y_OUTL);
	temp|=(uint16_t)MMA7455_read_byte(MMA7455_Y_OUTH);
	temp=temp<<8;
	temp|=buff;
	*y=(temp^0x1000)&0x1FFF;
	
	temp=0;buff=0;
	buff=MMA7455_read_byte(MMA7455_Z_OUTL);
	temp|=(uint16_t)MMA7455_read_byte(MMA7455_Z_OUTH);
	temp=temp<<8;
	temp|=buff;
	*z=(temp^0x1000)&0x1FFF;
}
