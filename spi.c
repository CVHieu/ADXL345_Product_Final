/* ---------------------------------------------------------------------------
	SPI source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"
	
void spi_init(uint8_t mode,uint8_t dord,uint8_t interrupt,uint8_t clock){
	// specify pin directions for SPI pins on port B
	if(clock==SPI_SLAVE){ // if slave SS and SCK is input
		DDRB&=~(1<<SPI_MOSI_PIN);	// input
		DDRB|=(1<<SPI_MISO_PIN);	// output
		DDRB&=~(1<<SPI_SS_PIN); 	// input
		DDRB&=~(1<<SPI_SCK_PIN);	// input
	} else {
		DDRB|=(1<<SPI_MOSI_PIN);	// output
		DDRB&=~(1<<SPI_MISO_PIN);	// input
		DDRB|=(1<<SPI_SCK_PIN); 	// output
		DDRB|=(1<<SPI_SS_PIN);		// output
	}
	SPCR=((interrupt?1:0)<<SPIE)	// interrupt enabled
	|(1<<SPE)					// enable SPI
	|(dord<<DORD)				// LSB or MSB
	|(((clock!=SPI_SLAVE)?1:0)<<MSTR)	// Slave or Master
	|(((mode&0x02)==2)<<CPOL)		// clock timing mode CPOL
	|(((mode&0x01))<<CPHA)		// clock timing mode CPHA
	|(((clock&0x02)==2)<<SPR1)	// cpu clock divisor SPR1
	|((clock&0x01)<<SPR0);			// cpu clock divisor SPR0
	SPSR=(((clock&0x04)==4)<<SPI2X); // clock divisor SPI2X
}

void disable_spi(){
	SPCR=0;
}

uint8_t send_spi(uint8_t out){
	SPDR=out;
	while(!(SPSR&(1<<SPIF)));
	return SPDR;
}

uint8_t received_from_spi(uint8_t data){
	SPDR=data;
	return SPDR;
}
