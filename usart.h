/* ---------------------------------------------------------------------------
	Header file for Usart
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#ifndef _USART_H_
#define _USART_H_

/* ---------------------------------------------------------------------------
	DEBUG FUNCTION ENABLED
--------------------------------------------------------------------------- */
#if _PROJ_DEBUG_

/* ------------------------- Extern functions ------------------------- */
void USART_Init (void);
unsigned char USART_Receive (void);
void USART_Transmit (unsigned char data);
unsigned char DataInReceiveBuffer (void);

/* ---------------------------------------------------------------------------
	DEBUG FUNCTION DISABLED
--------------------------------------------------------------------------- */
#else

#define TIME_OUT_USART	50		// x10ms = 5s
#define MAX_LEN_USART	6		// 6 byte
#define MIN_LEN_USART	2		// 2 byte

extern volatile uint8_t timeout_usart,en_timeout_usart,uart_error,timeout_usart_finish;
extern void ClrReceiveBuffer(void);


/* ------------------------- Extern functions ------------------------- */
void USART_Init(void);
unsigned char USART_Receive( void );
void USART_Transmit(unsigned char data );
unsigned char DataInReceiveBuffer(void);
void mpcm_send(unsigned char *dat,unsigned char len);
#endif

#endif // END _USART_H_
