/* ---------------------------------------------------------------------------
	Source file for Usart
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#include "machine.h"

/* ------------------------- UART Buffer Defines ------------------------- */
#define USART_RX_BUFFER_SIZE 128     /* 2,4,8,16,32,64,128 or 256 bytes */
#define USART_TX_BUFFER_SIZE 128     /* 2,4,8,16,32,64,128 or 256 bytes */
#define USART_RX_BUFFER_MASK (USART_RX_BUFFER_SIZE-1)
#define USART_TX_BUFFER_MASK (USART_TX_BUFFER_SIZE-1)
#if(USART_RX_BUFFER_SIZE&USART_RX_BUFFER_MASK)
	#error RX buffer size is not a power of 2
#endif
#if(USART_TX_BUFFER_SIZE&USART_TX_BUFFER_MASK)
	#error TX buffer size is not a power of 2
#endif

/* ------------------------- Static Variables ------------------------- */
/*
static uint8_t USART_RxBuf[USART_RX_BUFFER_SIZE];
static volatile uint8_t USART_RxHead;
static volatile uint8_t USART_RxTail;
static uint8_t USART_TxBuf[USART_TX_BUFFER_SIZE];
static volatile uint8_t USART_TxHead;
static volatile uint8_t USART_TxTail;
*/

/* ------------------------- Static Variables ------------------------- */
volatile unsigned char USART_RxBuf[USART_RX_BUFFER_SIZE];
volatile unsigned char USART_RxHead;
volatile unsigned char USART_RxTail;
volatile unsigned char USART_TxBuf[USART_TX_BUFFER_SIZE];
volatile unsigned char USART_TxHead;
volatile unsigned char USART_TxTail;


/* ---------------------------------------------------------------------------
	DEBUG FUNCTION ENABLED
--------------------------------------------------------------------------- */
#if _PROJ_DEBUG_
#include <stdio.h>

/* ------------------------- Function prototypes ------------------------- */
int uart_putchar(char c,FILE *unused);

/* ------------------------- Global Variables ------------------------- */
FILE mystdout=FDEV_SETUP_STREAM(uart_putchar,NULL,_FDEV_SETUP_WRITE);

/* ------------------------- Function references ------------------------- */
// Use fix-baudrate
// For debug mode: 115200
// For normal running: 38400
void USART_Init(void){
// Enable UART receiver and transmitter
// Set the baud rate
// 115200
	DDRD&=~(_BV(RXD_PIN));
	DDRD|=_BV(TXD_PIN);
	UCSRA=0x02;									// x2 speed
	UCSRC=(_BV(URSEL)|_BV(UCSZ1)|_BV(UCSZ0));		// 8 bit mode
	UBRRH=0x00;
	UBRRL=0x22;									// 57600
//	UBRRL=0x10;									// 115000
	UCSRB=((1<<RXCIE)|(1<<RXEN)|(1<<TXEN));

	// Flush receive buffer
	USART_RxTail=0;
	USART_RxHead=0;
	USART_TxTail=0;
	USART_TxHead=0;

	//Needed for debug!
	stdout=&mystdout;
}

// Receive complete
ISR(USART_RXC_vect){
	unsigned char data;
	unsigned char tmphead;
	// Read the received data
	data=UDR;
	// Calculate buffer index
	tmphead=(USART_RxHead+1)&USART_RX_BUFFER_MASK;
	USART_RxHead=tmphead;				/* Store new index */

	/*
	if (tmphead == USART_RxTail)
	{
		// ERROR! Receive buffer overflow 
		// This would never happen!!!  -->  Haizzz! :))
	}
	*/
	USART_RxBuf[tmphead]=data; 		/* Store received data in buffer */	
}

// USART Data Register Empty
ISR(USART_UDRE_vect){
	unsigned char tmptail;

	// Check if all data is transmitted
	if (USART_TxHead!=USART_TxTail){
		// Calculate buffer index
		tmptail = (USART_TxTail + 1)&USART_TX_BUFFER_MASK;
		USART_TxTail = tmptail;     	/* Store new index */
	
		UDR = USART_TxBuf[tmptail];  	/* Start transmition */
	}
	else{
		UCSRB &= ~(1<<UDRIE);   		/* Disable UDRE interrupt */
	}
}

// Read and write functions
unsigned char USART_Receive(void){
	unsigned char tmptail;
	// Wait for incomming data
	while(USART_RxHead==USART_RxTail);
	// Calculate buffer index
	tmptail=(USART_RxTail+1)&USART_RX_BUFFER_MASK;
	USART_RxTail=tmptail;             /* Store new index */
	return USART_RxBuf[tmptail];      	/* Return data */
}

// Transmit
void USART_Transmit(unsigned char data){
	unsigned char tmphead;
	// Calculate buffer index
	tmphead=(USART_TxHead+1)&USART_TX_BUFFER_MASK;
	// Wait for free space in buffer
//	while (tmphead==USART_RxTail)		Unnecessary
	USART_TxBuf[tmphead]=data;			/* Store datasadf */
	USART_TxHead=tmphead;             	/*Store new index */
	UCSRB|=(1<<UDRIE);                	/* Enable UDRE interrupt */
}

void USART_SendStr(char *ptr){
	while(*ptr){
		USART_Transmit(*ptr++);
	}
}

unsigned char DataInReceiveBuffer(void){
	return(USART_RxHead!=USART_RxTail);	/* Return 0 (FALSE) if the receive buffer is empty */
}


/* ---------------------------------------------------------------------------
	Send character c down the UART Tx, wait until tx holding register is empty.
--------------------------------------------------------------------------- */
int uart_putchar(char c, FILE *unused){
	if(c=='\n')uart_putchar('\r',0);
		USART_Transmit(c);
	return 0;
}


/* ---------------------------------------------------------------------------
 	DEBUG FUNCTION DISABLED
--------------------------------------------------------------------------- */
#else /* !Not debug*/

#include <stdio.h>

/* --------------------- Global variables ------------------- */
// Multi-processor Communication mode (MPCM)  variable: mpcm_phase
// 0: init (do nothing).
// 1: ID_Module.
// 2: len identified and dat. receive in processing.

volatile uint8_t mpcm_phase;
volatile uint8_t mpcm_dat_len;

void USART_Transmit(unsigned char data);


/* --------------------- Function references ------------------- */
/* Initialize USART */
// Use fix-baudrate
// For debug mode: 115200
// For normal running: 38400
void USART_Init(void){
	/* Set the baud rate */
	// 38400
	/* Enable UART receiver, transmitter, receive interrupt enable,  */
	UCSRA|=_BV(MPCM);								// MPCM mode
	UCSRC|=(_BV(URSEL)|_BV(UCSZ1)|_BV(UCSZ0));	// 8 bit mode
	UBRRH=0x00;
	UBRRL=0x19;									// 38400
	UCSRB|=(_BV(RXCIE)|_BV(RXEN)|_BV(TXEN)|_BV(UCSZ2)|_BV(TXCIE));
	DDRD&=~(_BV(RXD_PIN));
	DDRD|=_BV(TXD_PIN);
	/* Flush receive buffer */
	USART_RxTail=0;
	USART_RxHead=0;
	USART_TxTail=0;
	USART_TxHead=0;
	// Loc nhieu
	timeout_usart=0;
	// For MPCM
	mpcm_phase=0;
}

//void (*reset_cpu)(void)=0;

void ClrReceiveBuffer(void){
	cli();
	// Loc nhieu
	rs_uart_cnt++;
	if(rs_uart_cnt>1){
		//reset_cpu();
		while(1);
	}
	uart_error=0;
	timeout_usart=0;
	// For MPCM
	mpcm_phase=0;
	MPCM_release();
	UBRRH=0x00;
	UBRRL=0x19;									// 38400
	UCSRA|=_BV(MPCM);								// MPCM mode
	UCSRB|=(_BV(RXCIE)|_BV(RXEN)|_BV(TXEN)|_BV(UCSZ2)|_BV(TXCIE));
	UCSRB&=~(_BV(UDRIE));
	UCSRC|=(_BV(URSEL)|_BV(UCSZ1)|_BV(UCSZ0));	// 8 bit mode
	DDRD&=~(_BV(RXD_PIN));DDRD|=_BV(TXD_PIN);
	//PORTD|=(_BV(RXD_PIN)|_BV(TXD_PIN));
	USART_RxTail=0;
	USART_RxHead=0;
	USART_TxTail=0;
	USART_TxHead=0;
	reset_timer_task();
	//buzz_warning();			// For debug
	sei();
}


// Receive complete
ISR(USART_RXC_vect){
unsigned char data;
unsigned char tmphead;
unsigned char tmpUCSRB;
unsigned char accept;
	
	/* Read the received data */
	tmpUCSRB=UCSRB&(1<<RXB8);
	data=UDR;
	accept=0;
	// MPCM mode
	switch(mpcm_phase){
		case 0:
			// Ident. its own slave address
			if ((tmpUCSRB)&&(data==THIS_MODULE_ID_ADDR)){
				mpcm_phase=1;
				/*
				if(timeout_usart_finish){
					timeout_usart=0;
					en_timeout_usart=ENABLE;
					timeout_usart_finish=0;
				}
				*/
				UCSRA&=~(_BV(MPCM)); 	// Clear MPCM bit to receive more data
				accept=1;
			}
			break;
		case 1:
			// Get dat. len
			if((data<MIN_LEN_USART)||(data>MAX_LEN_USART)){	// Nhieu phat sinh trong qua trinh truyen
				uart_error=1;
				return;
			}
			mpcm_dat_len=data;
			mpcm_phase=2;
			accept=1;
			break;
		case 2:
			mpcm_dat_len--;
			if (mpcm_dat_len==0){
				mpcm_phase=0;
				UCSRA|=_BV(MPCM); 		// Data is enough, set MPCM again
			}
			accept=1;
			break;
		default:
			uart_error=1;
			return;
	}
	
	if (accept){
		// Receive data
		/* Calculate buffer index */
		tmphead=(USART_RxHead+1)&USART_RX_BUFFER_MASK;
		USART_RxHead=tmphead;      /* Store new index */
		USART_RxBuf[tmphead]=data; /* Store received data in buffer */
	}
}


// USART Data Register Empty
ISR(USART_UDRE_vect){
unsigned char tmptail;
	/* Check if all data is transmitted */
	if(USART_TxHead!=USART_TxTail){
		/* Calculate buffer index */
		tmptail=(USART_TxTail+1)&USART_TX_BUFFER_MASK;
		USART_TxTail=tmptail;      /* Store new index */
		UDR=USART_TxBuf[tmptail];  /* Start transmition */
	}
	else{
		UCSRB&=~(_BV(UDRIE));         /* Disable UDRE interrupt */
		//beep_off();
	}
}

ISR(USART_TXC_vect){
	set_485_rx();
	//beep_off();			// For debug
}


/* Read and write functions */
unsigned char USART_Receive(void){
unsigned char tmptail;
//	while (USART_RxHead == USART_RxTail);  /* Wait for incomming data */
	tmptail=(USART_RxTail+1)&USART_RX_BUFFER_MASK;/* Calculate buffer index */
	USART_RxTail=tmptail;                /* Store new index */
	return USART_RxBuf[tmptail];           /* Return data */
}


// Transmit
void USART_Transmit(unsigned char data){
unsigned char tmphead;
	/* Calculate buffer index */
	tmphead=(USART_TxHead+1)&USART_TX_BUFFER_MASK; /* Wait for free space in buffer */
	USART_TxBuf[tmphead]=data;           	/* Store data in buffer */
	USART_TxHead=tmphead;                	/* Store new index */
	UCSRB|=_BV(UDRIE);                   	/* Enable UDRE interrupt */
}
	
unsigned char DataInReceiveBuffer(void){
	return(USART_RxHead!=USART_RxTail); 	/* Return 0 (FALSE) if the receive buffer is empty */
}

// MPCM send data
void mpcm_send(unsigned char *dat,unsigned char len){
unsigned char n;
	set_485_tx();
	//en_timeout_usart=DISABLE;
	timeout_usart=0;
	rs_uart_cnt=0;
	for(n=0;n<len;n++){
		USART_Transmit(*dat++);
	}
	//UCSRB|=_BV(UDRIE);		//For test bug
}

#endif
