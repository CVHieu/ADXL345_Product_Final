/* ---------------------------------------------------------------------------
	EEPROM section source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"

/* ------------------------------------------------------------
	EEPROM variables defines
 * ------------------------------------------------------------ */

// Module alarm bitmask
uint8_t EEMEM eep_module_alarm_bitmask=MPCM_ALARM_BITMASK; 
uint8_t EEMEM eep_module_warning_bitmask=MPCM_WARNING_BITMASK;

// PIR
uint8_t EEMEM eep_pir_onboard_cnt=PIR_SAMPLING_CNT;
uint8_t EEMEM eep_pir_onboard_learn_cnt=LEARNING_PIR_CNT;

// LM35
uint16_t EEMEM eep_lm35_onboard_1_delta=LM35_O1_SAMPLING_DELTA;
uint16_t EEMEM eep_lm35_onboard_2_delta=LM35_O2_SAMPLING_DELTA;
uint16_t EEMEM eep_lm35_learn_o1=LM35_LEARNING;
uint16_t EEMEM eep_lm35_learn_o2=LM35_LEARNING;

// SMOKE
//uint16_t EEMEM eep_smoke_delta=SMOKE_SAMPLING_SP;

// MMA
uint16_t EEMEM eep_mma_learn[3]={MMA_LEANING,MMA_LEANING,MMA_LEANING};
uint16_t EEMEM eep_mma_delta=MMA_SAMPLING_DELTA;
uint8_t EEMEM eep_mma_sampling_setpoint=MMA_SAMPLING_CNT_DEFAULT;