/* ---------------------------------------------------------------------------
	SPI header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _SPI_H_
#define _SPI_H_
	
#include "machine.h"
	
	// IO - PORTB
#define SPI_SS_PIN 		4
#define SPI_MOSI_PIN 	5
#define SPI_MISO_PIN 	6
#define SPI_SCK_PIN 	7
	
	// SPI clock modes
#define SPI_MODE_0 0x00 	/* Sample (Rising) Setup (Falling) CPOL=0, CPHA=0 */
#define SPI_MODE_1 0x01 	/* Setup (Rising) Sample (Falling) CPOL=0, CPHA=1 */
#define SPI_MODE_2 0x02	 	/* Sample (Falling) Setup (Rising) CPOL=1, CPHA=0 */
#define SPI_MODE_3 0x03 	/* Setup (Falling) Sample (Rising) CPOL=1, CPHA=1 */
	
	// Data direction
#define SPI_LSB 1 	/* send least significant bit (bit 0) first */
#define SPI_MSB 0 	/* send most significant bit (bit 7) first */
	
	// Interrupt when data received (SPIF bit received)
#define SPI_NO_INTERRUPT 	0
#define SPI_INTERRUPT 		1
	
	// Slave or master with clock diviser
#define SPI_SLAVE 0xF0
#define SPI_MSTR_CLK4 0x00 		/* clock/4 */
#define SPI_MSTR_CLK16 0x01 	/* clock/16 */
#define SPI_MSTR_CLK64 0x02 	/* clock/64 */
#define SPI_MSTR_CLK128 0x03 	/* clock/128 */
#define SPI_MSTR_CLK2 0x04 		/* clock/2 */
#define SPI_MSTR_CLK8 0x05 		/* clock/8 */
#define SPI_MSTR_CLK32 0x06 	/* clock/32 */
	
// Init spi
void spi_init(uint8_t mode, 	// timing mode SPI_MODE[0-4]
			   uint8_t dord,		// data direction SPI_LSB|SPI_MSB
			   uint8_t interrupt,	// whether to raise interrupt on recieve
			   uint8_t clock);		// clock diviser

// setup spi
void setup_spi(uint8_t mode,   // timing mode SPI_MODE[0-4]
			   int dord,			 // data direction SPI_LSB|SPI_MSB
			   int interrupt,		 // whether to raise interrupt on recieve
			   uint8_t clock); // clock diviser

// disable spi
void disable_spi(void);

// send and receive a byte of data (master mode)
uint8_t send_spi(uint8_t out);

// receive the byte of data waiting on the SPI buffer and
// set the next byte to transfer - for use in slave mode
// when interrupts are enabled.
uint8_t received_from_spi(uint8_t data);

#endif
