/* ---------------------------------------------------------------------------
	Source file for ADC
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"

/* --------------------------------------------------
Note ADC Voltage Reference:
ADC_VREF_TYPE:
	0x00	AVREF PIN
	0x40	AVCC PIN (with capacitor at AREF pin)
	0xC0	IN_2.56V

Note ADC Prescaler:
ADCSRA:
	0x80	div 2
	0x81	div 2
	0x82	div 4
	0x83	div 8
	0x84	div 16
	0x85	div 32
	0x86	div 64
	0x87	div 128
-------------------------------------------------- */

void adc_init(void){
	// ADC Clock frequency: 125.000 kHz  -  XT 16MHz
	ADMUX=ADC_VREF_TYPE;
	ADCSRA=0x87;
}

// Read the AD conversion result
unsigned int read_adc(unsigned char adc_input){
	ADMUX=adc_input|ADC_VREF_TYPE;
	_delay_us(10);
	// Start the AD conversion
	ADCSRA|=0x40;
	// Wait for the AD conversion to complete
	while((ADCSRA&0x10)==0);
	ADCSRA|=0x10;
	return(ADCW&0x3FF);		// ADC 10bit
}
