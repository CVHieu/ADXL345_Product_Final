/* ---------------------------------------------------------------------------
	EEPROM section header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _PROJ_EEPROM_H_
#define _PROJ_EEPROM_H_


/* -------------- Extern variables --------------------- */
// MLX
//extern uint8_t EEMEM eep_mlx_sp; 

// Module alarm bitmask
extern uint8_t EEMEM eep_module_alarm_bitmask;
extern uint8_t EEMEM eep_module_warning_bitmask;

// PIR
extern uint8_t EEMEM eep_pir_onboard_cnt;
extern uint8_t EEMEM eep_pir_onboard_learn_cnt;

// MMA
extern uint16_t EEMEM eep_mma_learn[3];
extern uint16_t EEMEM eep_mma_delta;
extern uint8_t EEMEM eep_mma_sampling_setpoint;

// LM35
extern uint16_t EEMEM eep_lm35_onboard_1_delta;
extern uint16_t EEMEM eep_lm35_onboard_2_delta;
extern uint16_t EEMEM eep_lm35_learn_o1;
extern uint16_t EEMEM eep_lm35_learn_o2;

// SMOKE
//extern uint16_t EEMEM eep_smoke_delta;

#endif
