/* ---------------------------------------------------------------------------
	IO for module Sensor
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#ifndef _IO_DEFINE_H_
#define _IO_DEFINE_H_

// Module header file include
#include "machine.h"

/* ------------------------- IO define -------------------------*/

// ---------- ADC channel ----------
#define ADC_LM35_ONBOARD_1	3
#define ADC_LM35_ONBOARD_2	4
// Update:
#define ADC_HUMIDITY		6
// Straping
#define ADC_SHAKE	ADC_HUMIDITY


// ---------- Port B ----------
#define SMOKE_PIN			2	// IN - HI ACTIVE
// Update:


// ---------- Port C ----------
#define BUZZER_PIN 			6	// OUT - LOW ACTIVE
#define LED_STATUS_PIN		5	// OUT - HIGH ACTIVE
#define RW485_PIN			2

// Update:


// ---------- Port D ----------
#define RXD_PIN				0
#define TXD_PIN				1
#define PIR_PIN				2	// IN - LOW ACTIVE
#define LIMITSW_PIN			4	// IN - LOW ACTIVE
#define BUTTON_PIN			5	// IN - LOW ACTIVE
// Update:


/* ------------------------- Util macros for IO ------------------------- */
#define beep_on() 				{PORTC&=~(_BV(BUZZER_PIN));}
#define beep_off() 				{PORTC|=(_BV(BUZZER_PIN));}
#define buzz_warning() 			{PORTC^=_BV(BUZZER_PIN);}
#define led_status_flashing() 	PORTC^=_BV(LED_STATUS_PIN);

#define pir_check()				((!(PIND&_BV(PIR_PIN)))?1:0)
#define limitsw_check() 		((PIND&_BV(LIMITSW_PIN))?1:0)
#define smoke_check()			((PINB&_BV(SMOKE_PIN))?0:1)
#define button_on()			((PIND&_BV(BUTTON_PIN))?0:1)

#define select_mma()			{PORTB&=~(1<<SPI_SS_PIN);}
#define deselect_mma()			{PORTB|=(1<<SPI_SS_PIN);}

#define set_485_rx()			{PORTC&=~(_BV(RW485_PIN));}
#define set_485_tx()			{PORTC|=(_BV(RW485_PIN));}


#endif
