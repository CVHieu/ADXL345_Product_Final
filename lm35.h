/* ---------------------------------------------------------------------------
	Header file for LM35 sensor
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#ifndef _LM35_H_
#define _LM35_H_

#define lm35_to_temp(x)	((float)x/3.1)		//  Vref 3V3

void read_lm35_temp(uint8_t add,uint8_t *x);

#endif
