/* ---------------------------------------------------------------------------
	GPIO config header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#ifndef _GPIO_H_
#define _GPIO_H_

/*---------- Functions  ----------*/
void delay_ms(uint16_t time);
void beep (unsigned char times);
void beep_delay (unsigned char loop,unsigned int time);
void beep_long(void);

#endif
