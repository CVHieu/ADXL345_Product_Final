/* ---------------------------------------------------------------------------
	Header file for ADC
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _ADC_H_
#define _ADC_H_

#define ADC_VREF_TYPE 0x00	// ADC Voltage Reference: VREF PIN  -  3V3

void adc_init(void);
unsigned int read_adc(unsigned char adc_input);

#endif