/* ---------------------------------------------------------------------------
	External interrupt source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
 	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#include "machine.h"
// Init external interrupt
// External interrupt info:
// INT0: PIR ONBOARD
// INT2: SMOKE
void eint_init(void){
/*
	// External Interrupt(s) initialization
	// INT0: On
	// INT0 Mode: Any change
	// INT2: On
	// INT2 Mode: Falling
	GICR|=(_BV(INT2)|_BV(INT0)|_BV(INT1)); 		// Cho phep ngat
	MCUCR|=(_BV(ISC00)|_BV(ISC11));			// Falling INT1; Any INT0
	MCUCSR|=_BV(ISC2);							// Rising INT2
	GIFR|=(_BV(INTF2)|_BV(INTF0)|_BV(INTF1));
*/
	// External Interrupt(s) initialization
	// INT0: On
	// INT0 Mode: Any change
	// INT2: On
	// INT2 Mode: Falling
	GICR|=(_BV(INT0)|_BV(INT2)); 		// Cho phep ngat
	MCUCR|=_BV(ISC01);					// Falling INT0
	MCUCSR&=~_BV(ISC2);					// Falling INT2
	GIFR|=(_BV(INTF0)|_BV(INTF2));		// Clear interrupt flag
}


// External interrupt 0 routine
// PIR extern
ISR(INT0_vect){
	if(en_pir){
		if(!pir_o.en){
			pir_o.en=1;
			pir_o.cnt=PIR_SAMPLING_TIME;
			if(!flag_en_pir){
				flag_en_pir=1;
			}
		}

		// For learning pir offset
		if(!learning_pir_o.flag){
			learning_pir_o.time=PIR_SAMPLING_CNT_TIME;
			learning_pir_o.cnt=0;
			learning_pir_o.flag=1;
		}
		// end
	}
}

// External interrupt 2 routine
// SMOKE
ISR(INT2_vect){
	if(!smoke.en){
		smoke.en=1;
		smoke.cnt=0;
	}
}
