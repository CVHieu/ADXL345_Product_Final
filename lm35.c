/* ---------------------------------------------------------------------------
	Source file for LM35 sensor
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#include "machine.h"

// Read ADC value from LM35
void read_lm35_temp (uint8_t add,uint8_t *x){
	uint16_t temp;
	temp=read_adc(add);
	*x=lm35_to_temp(temp);
}
