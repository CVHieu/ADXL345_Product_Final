/* ---------------------------------------------------------------------------
	Debug configuration header file for entire FIS project
	MCU ATmega32	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#ifndef _DEBUG_H
#define _DEBUG_H

#define _PROJ_DEBUG_		0	// 1 debug, 0 run

#endif
