/* ---------------------------------------------------------------------------
	Source file module Sensor
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#include "machine.h"

/* ---------------------------------------------------------------------------
	Global variables
--------------------------------------------------------------------------- */
// Global sensor states
volatile uint8_t allSensorsStat;
volatile uint8_t module_alarm_state,module_warning_state,module_error_state;
volatile uint8_t en_pir=0,ac_detect=0;
// v6: flag_en_shake
volatile uint8_t flag_en_pir,flag_en_mma; // flag_en_shake;
volatile uint8_t sys_runmode,rs_uart_cnt;
volatile uint8_t module_alarm_bitmask;
volatile uint8_t module_warning_bitmask;
volatile uint8_t uart_error=0,timeout_usart;
// v6: timer_task_1s_shake
uint8_t timer_task_1s_pir,timer_task_1s_mma; // timer_task_1s_shake;
uint8_t buzz_warning_cnt;
uint8_t sensor_warning_dis;

struct OnOffSensorSampling_Type pir_o;
struct OnOffSensorSampling_Type limitsw;
struct OnOffSensorSampling_Type smoke;
struct ADCSensorSampling_Type lm35_o1;
struct ADCSensorSampling_Type lm35_o2;
struct MMASampling_Type mma;
// v6: crosscheck MMA w/ shake
// struct ShakeSensorSampling_Type shake;
struct Learning_mma_Type learning_mma;
struct Learning_lm35_Type learning_lm35_o1;
struct Learning_lm35_Type learning_lm35_o2;
struct Learning_pir_Type learning_pir_o;
union con_byte convert;

void main_init(void){
	/* ----- Port A ----- */
	DDRA&=~(_BV(ADC_LM35_ONBOARD_1)|_BV(ADC_LM35_ONBOARD_2)|_BV(ADC_HUMIDITY));
	PORTA&=~(_BV(ADC_LM35_ONBOARD_1)|_BV(ADC_LM35_ONBOARD_2)|_BV(ADC_HUMIDITY));

	/* ----- Port B ----- */
	DDRB&=~(_BV(SMOKE_PIN));
	PORTB&=~(_BV(SMOKE_PIN));
	
	/* ----- Port C ----- */
	DDRC|=(_BV(BUZZER_PIN)|_BV(LED_STATUS_PIN)|_BV(RW485_PIN));
	PORTC&=~(_BV(LED_STATUS_PIN)|_BV(RW485_PIN));
	PORTC|=_BV(BUZZER_PIN);
	
	/* ----- Port D ----- */
	DDRD&=~(_BV(BUTTON_PIN)|_BV(LIMITSW_PIN)|_BV(PIR_PIN));
	PORTD|=(_BV(BUTTON_PIN)|_BV(LIMITSW_PIN)|_BV(PIR_PIN));

	// usart init.
	USART_Init();
	// ADC init
	adc_init();
	// Timer init
	timer1_init();
	// Ex_int
	eint_init();
	//MMA
	init_MMA7455();
	// WDT init
	wdt_enable(WDTO_2S);
	wdt_reset();
	// Comparator off
	ACSR|=_BV(ACD);
	module_alarm_state=0;
	module_error_state=0;
	module_warning_state=0;
	buzz_warning_cnt=0;
	sensor_warning_dis=0;
}


// Reset sensor sampling state
void reset_sensor_sampling(void *ss, uint8_t ssType){
	switch (ssType){
		case SENSOR_TYPE_ONOFF:
			((struct OnOffSensorSampling_Type*)ss)->en=0;
			((struct OnOffSensorSampling_Type*)ss)->alarm=0;
			((struct OnOffSensorSampling_Type*)ss)->samplingsp=0;
			break;
		case SENSOR_TYPE_ADC:
			((struct ADCSensorSampling_Type*)ss)->finish=0;
			((struct ADCSensorSampling_Type*)ss)->data=0;
			((struct ADCSensorSampling_Type*)ss)->cnt=0;
			break;
		case SENSOR_TYPE_MMA:
			((struct MMASampling_Type*)ss)->flag.bf.finish=0;
			((struct MMASampling_Type*)ss)->flag.bf.begin=1;	// for begin sampling mma
			((struct MMASampling_Type*)ss)->cnt=0;
			((struct MMASampling_Type*)ss)->data_x=0;
			((struct MMASampling_Type*)ss)->data_y=0;
			((struct MMASampling_Type*)ss)->data_z=0;
			((struct MMASampling_Type*)ss)->data_x_buff=0;
			((struct MMASampling_Type*)ss)->data_y_buff=0;
			((struct MMASampling_Type*)ss)->data_z_buff=0;
			((struct MMASampling_Type*)ss)->data_x_min=0;
			((struct MMASampling_Type*)ss)->data_x_max=0;
			((struct MMASampling_Type*)ss)->data_y_min=0;
			((struct MMASampling_Type*)ss)->data_y_max=0;
			((struct MMASampling_Type*)ss)->data_z_min=0;
			((struct MMASampling_Type*)ss)->data_z_max=0;
			((struct MMASampling_Type*)ss)->samplingsp=0;
			break;
		case LEARNING_MMA:
			break;
		case LEARNING_LM35:
			((struct Learning_lm35_Type*)ss)->finish=0;
			((struct Learning_lm35_Type*)ss)->cnt=0;
			break;
		case LEARNING_PIR:
			((struct Learning_pir_Type*)ss)->flag=0;
			((struct Learning_pir_Type*)ss)->time=0;
			((struct Learning_pir_Type*)ss)->cnt=0;
			break;
		/*
		// v6: shake
		case SENSOR_TYPE_SHAKE:
			((struct ShakeSensorSampling_Type*)ss)->flag.bf.finish=0;
			((struct ShakeSensorSampling_Type*)ss)->flag.bf.begin=1; // for begin sampling shake
			((struct ShakeSensorSampling_Type*)ss)->data=0;
			((struct ShakeSensorSampling_Type*)ss)->cnt=0;
			((struct ShakeSensorSampling_Type*)ss)->data_buff=0;
			((struct ShakeSensorSampling_Type*)ss)->data_min=0;
			((struct ShakeSensorSampling_Type*)ss)->data_max=0;
			((struct ShakeSensorSampling_Type*)ss)->deltaSPCnt=0;
			break;
		*/
		default:
			// Do nothing
			break;
	}
}


// Reset module alarm state
void reset_module_alarm_state (void){
	if (module_alarm_state){
		beep_off();
		module_alarm_state=0;
	}
	allSensorsStat=0;
	reset_sensor_sampling(&pir_o,SENSOR_TYPE_ONOFF);
	reset_sensor_sampling(&limitsw,SENSOR_TYPE_ONOFF);
	reset_sensor_sampling(&smoke,SENSOR_TYPE_ONOFF);
	reset_sensor_sampling(&lm35_o1,SENSOR_TYPE_ADC);
	reset_sensor_sampling(&lm35_o2,SENSOR_TYPE_ADC);
	reset_sensor_sampling(&mma,SENSOR_TYPE_MMA);
	// v6: shake
	// reset_sensor_sampling(&shake,SENSOR_TYPE_SHAKE);	
	reset_sensor_sampling(&learning_mma,LEARNING_MMA);
	reset_sensor_sampling(&learning_lm35_o1,LEARNING_LM35);
	reset_sensor_sampling(&learning_lm35_o2,LEARNING_LM35);
	reset_sensor_sampling(&learning_pir_o,LEARNING_PIR);

	timer_task_1s_pir=0;
	timer_task_1s_mma=0;
	flag_en_mma=0;
	flag_en_pir=0;
	// v6: shake
	//timer_task_1s_shake = 0;
	//flag_en_shake = 0;
}

void average_mma(void){
	uint8_t i,j;
	uint16_t temp;
	for (i=0;i<=7;i++){
		for(j=8;j>i;j--){
			if (mma.buff_x[j-1]>mma.buff_x[j]){
				temp=mma.buff_x[j-1];
				mma.buff_x[j-1]=mma.buff_x[j];
				mma.buff_x[j]=temp;
			}
			if (mma.buff_y[j-1]>mma.buff_y[j]){
				temp=mma.buff_y[j-1];
				mma.buff_y[j-1]=mma.buff_y[j];
				mma.buff_y[j]=temp;
			}
			if (mma.buff_z[j-1]>mma.buff_z[j]){
				temp=mma.buff_z[j-1];
				mma.buff_z[j-1]=mma.buff_z[j];
				mma.buff_z[j]=temp;
			}
		}
	}
	mma.dem=(mma.buff_x[3]+mma.buff_x[4]+mma.buff_x[5])/3;
	mma.data_x_buff=(uint16_t)((mma.dem*128)/512);
	mma.dem=(mma.buff_y[3]+mma.buff_y[4]+mma.buff_y[5])/3;
	mma.data_y_buff=(uint16_t)((mma.dem*128)/512);
	mma.dem=(mma.buff_z[3]+mma.buff_z[4]+mma.buff_z[5])/3;
	mma.data_z_buff=(uint16_t)((mma.dem*128)/512);
}

/*
// v6:
void average_shake(void){
	uint8_t i,j;
	uint16_t temp;
	
	for (i=0;i<=7;i++){
		for(j=8;j>i;j--){
			if (shake.buff[j-1]>shake.buff[j]){
				temp=shake.buff[j-1];
				shake.buff[j-1]=shake.buff[j];
				shake.buff[j]=temp;
			}
		}
	}
	shake.data_buff=(shake.buff[3]+shake.buff[4]+shake.buff[5])/3;
}
*/

void average_lm35(void *ss){
	uint8_t i,j;
	uint8_t temp_lm35;
	for (i=0;i<=7;i++){
		for(j=8;j>i;j--){
			if (((struct ADCSensorSampling_Type*)ss)->buff[j-1]>((struct ADCSensorSampling_Type*)ss)->buff[j]){
				temp_lm35=((struct ADCSensorSampling_Type*)ss)->buff[j-1];
				((struct ADCSensorSampling_Type*)ss)->buff[j-1]=((struct ADCSensorSampling_Type*)ss)->buff[j];
				((struct ADCSensorSampling_Type*)ss)->buff[j]=temp_lm35;
			}
		}
	}
	((struct ADCSensorSampling_Type*)ss)->data=(((struct ADCSensorSampling_Type*)ss)->buff[3]\
												+((struct ADCSensorSampling_Type*)ss)->buff[4]\
												+((struct ADCSensorSampling_Type*)ss)->buff[5])/3;
}



/* ------------------------------------------
	User task function
------------------------------------------ */
void user_task_10ms(void){
	if(uart_error){
		ClrReceiveBuffer();
	}
	if (sys_runmode==SYS_RUNMODE_CONFIG){return;}

	/* ------------- PIR onboard sampling ------------------- */
	if(pir_o.en&&en_pir){
		if(pir_check()){
			if(pir_o.cnt){pir_o.cnt--;}
			if(pir_o.cnt==0){
				pir_o.en=0;
				pir_o.samplingsp++;
				if(pir_o.samplingsp>=pir_o.cntsp){
					pir_o.alarm=1;
				}
				// For learning pir offset
				if((sys_runmode==SYS_RUNMODE_TEST)&&learning_pir_o.flag){
					learning_pir_o.cnt++;
				}
				// End
			}
		}
		else{
			pir_o.en=0; 	// Bo qua, xem nhu nhieu
		}
	}


	/* ------------- SMOKE sampling ------------------- */
	if(smoke.en){
		if(smoke_check()){
			smoke.cnt++;
			if (smoke.cnt==SMOKE_SAMPLING_TIME){
				smoke.cnt=0;
				smoke.alarm=1;
				smoke.en=0;
			}
		}
		else{
			smoke.en=0;
			smoke.alarm=0;
			smoke.cnt=0;
		}
	}

	/* ------------- Limitsw sampling ------------------- */
	// v6: don't need limitsw.en, unused!
	//if (limitsw.en||limitsw_check()){
	//	limitsw.en=1;
	if(limitsw_check()){
		limitsw.cnt++;	
		if (limitsw.cnt==LIMITSW_SAMPLING_TIME){
			limitsw.cnt=0;
			limitsw.alarm=1;
			//limitsw.en=0;
		}
	}
	else{
		//limitsw.en=0;
		limitsw.alarm=0;
		limitsw.cnt=0;
	}
	//}

	if(module_warning_state){
		return;
	}

	/* --------------- MMA sampling ----------------- */
	if((!mma.flag.bf.finish)&&(!mma.flag.bf.error)&&(module_alarm_state==0)){
		read_MMA7455(&mma.buff_x[mma.cnt],&mma.buff_y[mma.cnt],&mma.buff_z[mma.cnt]);
		mma.cnt++;
		if (mma.cnt>8){
			mma.cnt=0;
			mma.flag.bf.finish=1;
		}
	}
	
	// v6:
	/* --------------- Shake sampling ----------------- */
	/*
	if (!shake.flag.bf.finish){
		shake.buff[shake.cnt] = read_adc(ADC_SHAKE);
		shake.cnt++;
		if (shake.cnt>8){
			shake.cnt=0;
			shake.flag.bf.finish=1;
		}
	}
	*/
	
	/* ------------- LM35 onboard 1 sampling ------------------- */
	if (!lm35_o1.finish){
		read_lm35_temp(ADC_LM35_ONBOARD_1,&lm35_o1.buff[lm35_o1.cnt]);
		lm35_o1.cnt++;
		if (lm35_o1.cnt>8){
			lm35_o1.cnt=0;
			lm35_o1.finish=1;
		}
	}

	/* ------------- LM35 onboard 2 sampling ------------------- */
	if (!lm35_o2.finish){
		read_lm35_temp(ADC_LM35_ONBOARD_2,&lm35_o2.buff[lm35_o2.cnt]);
		lm35_o2.cnt++;
		if (lm35_o2.cnt>8){
			lm35_o2.cnt=0;
			lm35_o2.finish=1;
		}
	}
}


void user_task_100ms(void){
	wdt_reset();
	timeout_usart++;
	if(timeout_usart>TIME_OUT_USART){
		uart_error=1;
		return;
	}
	if(sys_runmode==SYS_RUNMODE_CONFIG){goto task100ms_mpcm_process_label;}
	
	/* --------------- Limitsw alarm -------------- */
	if(limitsw.alarm){
		allSensorsStat|=_BV(MPCM_BITSTAT_LIMITSW);
		limitsw.alarm=0;
	}

	// v6: change LM35 prio. order higher than smoke and pir!
	/* ---------------- LM35 onboard 1 alarm --------------- */
	if(lm35_o1.finish){
		average_lm35(&lm35_o1);
		// Neu dang chay mode test se thuc hien khoi lenh nay
		if(sys_runmode==SYS_RUNMODE_TEST){
			if(learning_lm35_o1.cnt<10){
				learning_lm35_o1.buff[learning_lm35_o1.cnt]=lm35_o1.data;
				learning_lm35_o1.cnt++;
			}
			else{
				if(!learning_lm35_o1.finish){
					learning_lm35_o1.finish=1;
				}
			}
		}
		if(lm35_o1.data>=(uint8_t)lm35_o1.samplingsp){
			allSensorsStat|=_BV(MPCM_BITSTAT_LM35_O1);
		}
		lm35_o1.finish=0;
	}

	/* ---------------- LM35 onboard 2 alarm --------------- */
	if(lm35_o2.finish){
		average_lm35(&lm35_o2);
		// Neu dang chay mode test se thuc hien khoi lenh nay
		if(sys_runmode==SYS_RUNMODE_TEST){
			if(learning_lm35_o2.cnt<10){
				learning_lm35_o2.buff[learning_lm35_o2.cnt]=lm35_o2.data;
				learning_lm35_o2.cnt++;
			}
			else{
				if(!learning_lm35_o2.finish){
					learning_lm35_o2.finish=1;
				}
			}
		}
		if(lm35_o2.data>=(uint8_t)lm35_o2.samplingsp){
			allSensorsStat|=_BV(MPCM_BITSTAT_LM35_O2);
			lm35_o2.data=0;
		}
		lm35_o2.finish=0;
	}

	/* ---------------- Smoke alarm --------------- */
	if(smoke.alarm){
		// allSensorsStat|=_BV(MPCM_BITSTAT_SMOKE);
		// v6: cross check w/ temp
		if ((lm35_o1.data >= (uint8_t)lm35_o1.smoke_pir_crosscheck_sp) || \
			(lm35_o2.data >= (uint8_t)lm35_o2.smoke_pir_crosscheck_sp) || \
			(allSensorsStat & _BV(MPCM_BITSTAT_LM35_O1)) || \
			(allSensorsStat & _BV(MPCM_BITSTAT_LM35_O2)))
			allSensorsStat|=_BV(MPCM_BITSTAT_SMOKE);
		smoke.alarm=0;
	}	

	/* --------------- PIR_O alarm -------------- */
	if(pir_o.alarm){
		// allSensorsStat|=_BV(MPCM_BITSTAT_PIR_O);
		// v6: cross check w/ temp
		if ((lm35_o1.data >= (uint8_t)lm35_o1.smoke_pir_crosscheck_sp) || \
			(lm35_o2.data >= (uint8_t)lm35_o2.smoke_pir_crosscheck_sp) || \
			(allSensorsStat & _BV(MPCM_BITSTAT_LM35_O1)) || \
			(allSensorsStat & _BV(MPCM_BITSTAT_LM35_O2))) 
			allSensorsStat|=_BV(MPCM_BITSTAT_PIR_O);
		// v6: corrected!
		// pir_o.cnt=PIR_SAMPLING_TIME;
		pir_o.alarm=0;
	}

	if (module_warning_state){
		goto task100ms_mpcm_process_label;
	}

	// v6: Shake alarm sampling is higher than MMA (for cross-checking)
	/* ---------------- Shake alarm --------------- */
	/*
	if(shake.flag.bf.finish){
		average_shake();
		if(shake.flag.bf.begin){
			shake.flag.bf.begin=0;
			shake.data_min=shake.data_buff;
			shake.data_max=shake.data_buff;
		}
		else{
			if(shake.data_buff<shake.data_min){
				shake.data_min=shake.data_buff;
			}
			if(shake.data_buff>shake.data_max){
				shake.data_max=shake.data_buff;
			}
		}
		shake.flag.bf.finish=0;
	}
	
	// TODO: this is bug: must place in task_1s 
	if(shake.deltaSPCnt>=shake.deltaSPCntMax){
		shake.alarm = 1;
	}
	*/
	
	/* ---------------- MMA alarm --------------- */
	if(mma.flag.bf.finish){
		average_mma();
		if(mma.flag.bf.begin){
			mma.flag.bf.begin=0;
			mma.data_x_min=mma.data_x_buff;
			mma.data_x_max=mma.data_x_buff;
			mma.data_y_min=mma.data_y_buff;
			mma.data_y_max=mma.data_y_buff;
			mma.data_z_min=mma.data_z_buff;
			mma.data_z_max=mma.data_z_buff;
		}
		else{
			if(mma.data_x_buff<mma.data_x_min){
				mma.data_x_min=mma.data_x_buff;
			}
			if(mma.data_x_buff>mma.data_x_max){
				mma.data_x_max=mma.data_x_buff;
			}
			if(mma.data_y_buff<mma.data_y_min){
				mma.data_y_min=mma.data_y_buff;
			}
			if(mma.data_y_buff>mma.data_y_max){
				mma.data_y_max=mma.data_y_buff;
			}
			if(mma.data_z_buff<mma.data_z_min){
				mma.data_z_min=mma.data_z_buff;
			}
			if(mma.data_z_buff>mma.data_z_max){
				mma.data_z_max=mma.data_z_buff;
			}
		}
		mma.flag.bf.finish=0;
	}

	// v6: cross-checking w/ shake
	if(mma.samplingsp>=mma.sampling_setpoint){
		allSensorsStat|=_BV(MPCM_BITSTAT_MMA);
	}
	/*
	// Shake cross check
	if(mma.samplingsp>=mma.sampling_setpoint){
		if (shake.alarm){
			allSensorsStat|=_BV(MPCM_BITSTAT_MMA);
		}
	}
	*/
	
	// Warning: reset shake.alarm in all cases to reset cross-checking
	// shake.alarm = 0;
	
task100ms_mpcm_process_label:

	if(allSensorsStat&module_alarm_bitmask){
		// Enter alarm state:
		module_alarm_state=1;
		beep_on();
	}
	else{
		if(!module_warning_state&&(!sensor_warning_dis)){
			if(allSensorsStat&module_warning_bitmask){
				module_warning_state=1;
				sensor_warning_dis=WARNING_TIME_DIS;
				buzz_warning_cnt=WARNING_BUZZ_CNT;
			}
		}
	}
	if(module_warning_state&&(!module_alarm_state)){
		buzz_warning();
		buzz_warning_cnt--;
		if(buzz_warning_cnt==0){
			beep_off();
			module_warning_state=0;
			allSensorsStat&=~module_warning_bitmask;
			delay_ms(200);
			reset_timer_task();
		}
	}
#if(!_PROJ_DEBUG_)
	MPCM_Process_Data();
	MPCM_Response();
#endif
}


void user_task_1s(void){
uint8_t tmp_8;
uint16_t tmp_16[3];
	led_status_flashing();
	/* ---------------- Check sensor error --------------- */
	// Neu chinh sua schematic them dien tro pull-down LM35
	// Check error LM35 exter & onboard
	module_error_state=GOOD;					// set OK
	read_lm35_temp(ADC_LM35_ONBOARD_1,&tmp_8);
	if((tmp_8<10)||(tmp_8>(uint8_t)lm35_o1.samplingsp)){
		module_error_state=ERROR;
	}
	read_lm35_temp(ADC_LM35_ONBOARD_2,&tmp_8);
	if((tmp_8<10)||(tmp_8>(uint8_t)lm35_o2.samplingsp)){
		module_error_state=ERROR;
	}
	// Check error MMA
	if(mma.flag.bf.error){
		module_error_state=ERROR;
	}

	if(sensor_warning_dis){
		sensor_warning_dis--;
	}

	// v6: shake is higher prio. than MMA
	/* ---------------- SHAKE --------------- */
	/*
	shake.data=(shake.data_max-shake.data_min);
	shake.flag.bf.begin=1;
	if(shake.data>=shake.deltaSP){
		if(shake.deltaSPCnt==0){flag_en_shake=1;}
		shake.deltaSPCnt++;
	}
	if (flag_en_shake){
		timer_task_1s_shake++;
		if (timer_task_1s_shake==MMA_SAMPLING_CNT_TIME){
			timer_task_1s_shake=0;
			flag_en_shake=0;
			shake.deltaSPCnt=0;
		}
	}
	*/
	
	/* ---------------- MMA --------------- */
	mma.data_x=(mma.data_x_max-mma.data_x_min);
	mma.data_y=(mma.data_y_max-mma.data_y_min);
	mma.data_z=(mma.data_z_max-mma.data_z_min);
	mma.flag.bf.begin=1;
	
	// Neu dang chay mode normal se thuc hien khoi lenh nay
	if((mma.data_x>=mma.sampling_delta[0])||(mma.data_y>=mma.sampling_delta[1])||\
		(mma.data_z>=mma.sampling_delta[2])){
		if(mma.samplingsp==0){flag_en_mma=1;}
		mma.samplingsp++;
	}
	
	if (flag_en_mma){
		timer_task_1s_mma++;
		if (timer_task_1s_mma==MMA_SAMPLING_CNT_TIME){
			timer_task_1s_mma=0;
			flag_en_mma=0;
			mma.samplingsp=0;
			// v6: MMA timeout controls for shake reset states also!
			//timer_task_1s_shake=0;
			//flag_en_shake=0;
			//shake.deltaSPCnt=0;
		}
	}
	// Neu dang chay mode test se thuc hien khoi lenh nay
	if(sys_runmode==SYS_RUNMODE_TEST){
		if(mma.data_x>learning_mma.data_x){
			learning_mma.data_x=mma.data_x;
		}
		if(mma.data_y>learning_mma.data_y){
			learning_mma.data_y=mma.data_y;
		}
		if(mma.data_z>learning_mma.data_z){
			learning_mma.data_z=mma.data_z;
		}
	}
	
	// LM35 onboard
	if(learning_lm35_o1.finish){
		tmp_16[0]=0;
		for(tmp_8=0;tmp_8<=learning_lm35_o1.cnt;tmp_8++){
			tmp_16[0]+=(uint16_t)learning_lm35_o1.buff[tmp_8];
		}
		tmp_8=tmp_16[0]/learning_lm35_o1.cnt;
		if(tmp_8>learning_lm35_o1.result){
			learning_lm35_o1.result=tmp_8;
		}
		learning_lm35_o1.cnt=0;
		learning_lm35_o1.finish=0;
	}
	
	// LM35 extern
	if(learning_lm35_o2.finish){
		tmp_16[0]=0;
		for(tmp_8=0;tmp_8<=learning_lm35_o2.cnt;tmp_8++){
			tmp_16[0]+=(uint16_t)learning_lm35_o2.buff[tmp_8];
		}
		tmp_8=tmp_16[0]/learning_lm35_o2.cnt;
		if(tmp_8>learning_lm35_o2.result){
			learning_lm35_o2.result=tmp_8;
		}
		learning_lm35_o2.cnt=0;
		learning_lm35_o2.finish=0;
	}
	
	// PIR
	if(learning_pir_o.time){
		learning_pir_o.time--;
		if(!learning_pir_o.time){
			learning_pir_o.flag=0;
			eeprom_busy_wait();
			tmp_8=eeprom_read_byte(&eep_pir_onboard_cnt);
			if(learning_pir_o.cnt>tmp_8){
				learning_pir_o.cnt=learning_pir_o.cnt-tmp_8;
			}
			else{
				learning_pir_o.cnt=0;
			}
			if(learning_pir_o.result<learning_pir_o.cnt){
				learning_pir_o.result=learning_pir_o.cnt;
			}
			learning_pir_o.cnt=0;
		}
	}

	/* ---------------- PIR --------------- */
	if (!en_pir){					/* Wait for PIR ok! */
		timer_task_1s_pir++;
		if (timer_task_1s_pir==PIR_EN_TIME){
			en_pir=1;
			flag_en_pir=0;
			timer_task_1s_pir=0;
		}
	}
	else{
		if (flag_en_pir){
			timer_task_1s_pir++;
			if (timer_task_1s_pir==PIR_SAMPLING_CNT_TIME){
				timer_task_1s_pir=0;
				flag_en_pir=0;
				pir_o.samplingsp=0;
			}
		}
	}

#if (_PROJ_DEBUG_)
	printf_P(PSTR("tmp_16 1: %d\n\r"),learning_mma.result[0]);_delay_us(100);
#endif
}

// v6: add for ADCSensorSampling_Type.smoke_pir_crosscheck_sp = learning + delta/4!
void init_value_eeprom(void){
	uint16_t temp;
	// LM35 onboard
	eeprom_busy_wait();
	temp = eeprom_read_word(&eep_lm35_onboard_1_delta);
	lm35_o1.samplingsp=temp;
	lm35_o1.smoke_pir_crosscheck_sp=temp/4;
	eeprom_busy_wait();
	temp=eeprom_read_word(&eep_lm35_learn_o1);
	lm35_o1.samplingsp+=temp;
	lm35_o1.smoke_pir_crosscheck_sp+=temp;
	// LM35 extern
	eeprom_busy_wait();
	temp=eeprom_read_word(&eep_lm35_onboard_2_delta);
	lm35_o2.samplingsp=temp;
	lm35_o2.smoke_pir_crosscheck_sp=temp/4;
	eeprom_busy_wait();
	temp=eeprom_read_word(&eep_lm35_learn_o2);
	lm35_o2.samplingsp+=temp;
	lm35_o2.smoke_pir_crosscheck_sp+=temp;
	
	// SMOKE
	//eeprom_busy_wait();
	//smoke.samplingsp=eeprom_read_word(&eep_smoke_delta);
	
	// MMA
	eeprom_busy_wait();
	mma.sampling_delta[0]=mma.sampling_delta[1]=mma.sampling_delta[2]=eeprom_read_word(&eep_mma_delta);
	eeprom_busy_wait();
	mma.sampling_delta[0]+=eeprom_read_word(&eep_mma_learn[0]);
	eeprom_busy_wait();
	mma.sampling_delta[1]+=eeprom_read_word(&eep_mma_learn[1]);
	eeprom_busy_wait();
	mma.sampling_delta[2]+=eeprom_read_word(&eep_mma_learn[2]);
	eeprom_busy_wait();
	mma.sampling_setpoint=eeprom_read_byte(&eep_mma_sampling_setpoint);

	// v6: shake
	// TODO: add these to eeprom space
	//shake.deltaSP = SHAKE_SAMPLING_DELTA;
	//shake.deltaSPCntMax = SHAKE_SAMPLING_DELTA_CNT_MAX;
	
	// Module alarm bitmask
	eeprom_busy_wait();
	module_alarm_bitmask=eeprom_read_byte(&eep_module_alarm_bitmask);
	eeprom_busy_wait();
	module_warning_bitmask=eeprom_read_byte(&eep_module_warning_bitmask);

	//PIR
	eeprom_busy_wait();
	pir_o.cntsp=eeprom_read_byte(&eep_pir_onboard_cnt);
	eeprom_busy_wait();
	pir_o.cntsp+=eeprom_read_byte(&eep_pir_onboard_learn_cnt);
}


// Load default machine operation state
// including default value loaded from eeprom 
void machine_state_init(void){
	init_value_eeprom();
#if(!_PROJ_DEBUG_)
	MPCM_Init();
#endif
	reset_module_alarm_state();
	module_alarm_state=0;
	sys_runmode=SYS_RUNMODE_CONFIG;
}


int main (void){
	main_init();
	machine_state_init();
	beep(5);
	sei();
#if (_PROJ_DEBUG_)
	printf_P(PSTR("Firmware debug\n\n\r"));_delay_us(100);
#endif
	while(1){
		simple_tasks_process();
	}
}
