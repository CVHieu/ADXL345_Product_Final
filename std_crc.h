/* ---------------------------------------------------------------------------
	Standard CRC calculation header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _STD_CRC_H_
#define _STD_CRC_H_

// Standard CRC calculation
uint8_t std_crc_calc(uint8_t message[], int nBytes, uint8_t polynomial);

#endif // _STD_CRC_H_