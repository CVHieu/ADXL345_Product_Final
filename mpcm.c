/* ---------------------------------------------------------------------------
	MPCM instruction set source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#include "machine.h"

/* ------------------------------------------
	Global variables section
------------------------------------------ */
volatile uint8_t master_request;
uint8_t data_in[32];
uint8_t data_in_cnt;
uint8_t data_in_len;
uint8_t data_out[32];

// 0: init.
// 1: addr matched
// 2: len received and data receiving...
uint8_t mpcm_process_stat;

// init MPCM
void MPCM_Init(void){
	master_request=0;
	mpcm_process_stat=0;
	data_in_cnt=0;
	data_in_len=0;
}

void MPCM_release(void){
	master_request=0;
	mpcm_process_stat=0;
}

// MPCM process
void MPCM_Process_Data(void){
	uint8_t tmp_data;
	mpcm_process_stat=0;
	while(DataInReceiveBuffer()){
		tmp_data=USART_Receive();
		switch (mpcm_process_stat){
			case 0:
				if(tmp_data==THIS_MODULE_ID_ADDR){
					mpcm_process_stat=1;
					data_in_cnt=0;
					data_in[data_in_cnt++]=tmp_data;
					//timeout_usart=0;
					//en_timeout_usart=ENABLE;
				}
				break;
			case 1:
				data_in[data_in_cnt++]=tmp_data;
				data_in_len=tmp_data;
				mpcm_process_stat=2;
				/*
				if((data_in_len<MIN_LEN_USART)||(data_in_len>MAX_LEN_USART)){
					ClrReceiveBuffer();
					MPCM_release();
					return;
				}
				else{
					mpcm_process_stat=2;
				}
				*/
				break;
			case 2:
				data_in[data_in_cnt++]=tmp_data;
				data_in_len--;
				if (data_in_len==0){
					mpcm_process_stat=0;
					master_request=1;
					//en_timeout_usart=DISABLE;
					//rx_cnt=0;
					//beep_on();delay_ms(50);
				}
				break;
			default:
				mpcm_process_stat=0;
				break;
		}
		if(master_request){break;}
	}
}


// Response master request
void MPCM_Response(void){
uint8_t bitmask;
uint8_t error_eep=0;
	//if(master_request){
	if(master_request&&(!uart_error)){
		master_request=0;
		// Check CRC
		if(std_crc_calc(data_in, data_in_cnt-1,CRC_MPCM_POLY)==data_in[data_in_cnt-1]){
			switch(data_in[MPCM_INS_CMD_OFS]){
				/* -------------------- General --------------------------- */
				// PING | RESET ALARM | NORMAL RUN | CONFIG RUN | Module Overal Status
				case MPCM_PING:
					if(sys_runmode!=SYS_RUNMODE_TEST){
						data_out[3]=(sys_runmode<<MODE_MASK)|(ac_detect<<AC_MASK)|(module_alarm_state<<\
							ALARM_MASK)|module_error_state;
					}
					else{
						data_out[3]=(ac_detect<<AC_MASK)|(module_alarm_state<<\
							ALARM_MASK)|module_error_state|(1<<TEST_MASK);
					}
					goto MPCM_send_1byte;
					
				case MPCM_RUNMODE_NORMAL:
					sys_runmode=SYS_RUNMODE_NORMAL;
					beep_off();
					delay_ms(100);
					beep_on();delay_ms(800);beep_off();
					delay_ms(200);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_RUNMODE_CONFIG:
					sys_runmode=SYS_RUNMODE_CONFIG;
					reset_module_alarm_state();
					beep_off();
					delay_ms(100);
					beep_delay(2,150);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_AC_ON:
					ac_detect=AC_ON;
					beep_off();
					delay_ms(100);
					beep_delay(2,150);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_AC_OFF:
					ac_detect=AC_OFF;
					beep_off();
					delay_ms(100);
					beep_delay(2,150);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_CLR_ALARM:
					reset_module_alarm_state();
					beep_off();
					delay_ms(100);
					beep_delay(1,150);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_WRITE_ALARM_BITMASK:
					bitmask=MPCM_ALL_BITMASK&(~data_in[3]);
					eeprom_busy_wait();
					eeprom_update_byte(&eep_module_alarm_bitmask,data_in[3]);
					eeprom_busy_wait();
					if(data_in[3]!=eeprom_read_byte(&eep_module_alarm_bitmask)){error_eep=1;}
					eeprom_busy_wait();
					eeprom_update_byte(&eep_module_warning_bitmask,bitmask);
					eeprom_busy_wait();
					if(bitmask!=eeprom_read_byte(&eep_module_warning_bitmask)){error_eep=1;}
					if(error_eep){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					module_alarm_bitmask=data_in[3];
					module_warning_bitmask=bitmask;
					reset_module_alarm_state();
					beep_off();
					delay_ms(100);
					beep_delay(3,100);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_READ_ALARM_BITMASK:
					eeprom_busy_wait();
					data_out[3]=eeprom_read_byte(&eep_module_alarm_bitmask);
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_1byte;

				case MPCM_RESTART_SENSOR:
					data_out[3]=MPCM_OK; 			/* Acknowledge! */
					// Resp stat for master
					data_out[0]=THIS_MODULE_ID_ADDR;
					data_out[1]=3;					// Length data
					data_out[2]=data_in[MPCM_INS_CMD_OFS];
					data_out[4]=std_crc_calc(data_out,4,CRC_MPCM_POLY);
					mpcm_send(&data_out[0],5);
					beep_off();
					//delay_ms(100);
					beep_delay(2,300);
					_delay_ms(100);					/* Noise Power Buzz*/
					while(1){}						// Restart by WDT
				
				case MPCM_RUNMODE_TEST:
					sys_runmode=SYS_RUNMODE_TEST;
					learning_mma.data_x=learning_mma.data_y=learning_mma.data_z=0;
					learning_lm35_o1.result=learning_lm35_o2.result=0;
					learning_pir_o.result=0;
					beep_off();
					delay_ms(100);
					beep_delay(2,150);
					delay_ms(100);					/* Noise Power Buzz*/
					reset_timer_task();
					goto MPCM_send_1byte_OK;


				case MPCM_FACTORY_RESET:
					beep_off();
					// LM35
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_onboard_1_delta,LM35_O1_SAMPLING_DELTA);
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_learn_o1,LM35_LEARNING);
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_onboard_2_delta,LM35_O2_SAMPLING_DELTA);
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_learn_o2,LM35_LEARNING);
					
					// SMOKE
					//eeprom_busy_wait();
					//eeprom_update_word(&eep_smoke_delta,SMOKE_SAMPLING_SP);
					
					// MMA
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_delta,MMA_SAMPLING_DELTA);
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[0],MMA_LEANING);
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[1],MMA_LEANING);
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[2],MMA_LEANING);
					eeprom_busy_wait();
					eeprom_update_byte(&eep_mma_sampling_setpoint,MMA_SAMPLING_CNT_DEFAULT);
					
					// Module alarm bitmask
					eeprom_busy_wait();
					module_alarm_bitmask=eeprom_read_byte(&eep_module_alarm_bitmask);
					eeprom_busy_wait();
					module_warning_bitmask=eeprom_read_byte(&eep_module_warning_bitmask);

					//PIR
					eeprom_busy_wait();
					eeprom_update_byte(&eep_pir_onboard_cnt,PIR_SAMPLING_CNT);
					eeprom_busy_wait();
					eeprom_update_byte(&eep_pir_onboard_learn_cnt,LEARNING_PIR_CNT);
					beep_delay(2,400);
					delay_ms(100);					/* Noise Power Buzz*/
					init_value_eeprom();
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				// Read operation
				case MPCM_READ_MODULE_STAT:
					// return global sensor status
					data_out[3]=allSensorsStat&module_alarm_bitmask;
					goto MPCM_send_1byte;

				case MPCM_READ_MMA_DELTA:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_mma_delta);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;
					
				case MPCM_READ_LM35_ON_1_DELTA:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_lm35_onboard_1_delta);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LM35_ON_2_DELTA:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_lm35_onboard_2_delta);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_SMOKE_DELTA:
					data_out[3]=0;
					data_out[4]=0;
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_PIR_CNT:
					data_out[3]=0;
					eeprom_busy_wait();
					data_out[4]=eeprom_read_byte(&eep_pir_onboard_cnt);
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_MMA_X:
					convert.i=learning_mma.data_x;
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_MMA_Y:
					convert.i=learning_mma.data_y;
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_MMA_Z:
					convert.i=learning_mma.data_z;
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_MMA_X:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_mma_learn[0]);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_MMA_Y:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_mma_learn[1]);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_MMA_Z:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_mma_learn[2]);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_LM35_O1:
					data_out[3]=0;
					data_out[4]=learning_lm35_o1.result;
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_LM35_O1:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_lm35_learn_o1);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_LM35_O2:
					data_out[3]=0;
					data_out[4]=learning_lm35_o2.result;
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_LM35_O2:
					eeprom_busy_wait();
					convert.i=eeprom_read_word(&eep_lm35_learn_o2);
					data_out[3]=convert.b[1];
					data_out[4]=convert.b[0];
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_LEARN_PIR_CNT:
					data_out[3]=0;
					data_out[4]=learning_pir_o.result;
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_OFS_PIR_CNT:
					data_out[3]=0;
					eeprom_busy_wait();
					data_out[4]=eeprom_read_byte(&eep_pir_onboard_learn_cnt);
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;

				case MPCM_READ_MMA_SAMPLING:
					data_out[3]=0;
					eeprom_busy_wait();
					data_out[4]=eeprom_read_byte(&eep_mma_sampling_setpoint);
					beep_delay(1,100);
					reset_timer_task();
					goto MPCM_send_2byte;


				// Write operation
				case MPCM_WRITE_MMA_DELTA:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>MMA_DELTA_MAX){
						convert.i=MMA_DELTA_MAX;
					}
					if(convert.i<MMA_DELTA_MIN){
						convert.i=MMA_DELTA_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_delta,convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_mma_delta)){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;
					
				case MPCM_WRITE_LM35_ON_1_DELTA:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>LM35_DELTA_MAX){
						convert.i=LM35_DELTA_MAX;
					}
					if(convert.i<LM35_DELTA_MIN){
						convert.i=LM35_DELTA_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_onboard_1_delta,convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_lm35_onboard_1_delta)){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;
					
				case MPCM_WRITE_LM35_ON_2_DELTA:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>LM35_DELTA_MAX){
						convert.i=LM35_DELTA_MAX;
					}
					if(convert.i<LM35_DELTA_MIN){
						convert.i=LM35_DELTA_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_onboard_2_delta,convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_lm35_onboard_2_delta)){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_SMOKE_DELTA:
					// Do nothing
					beep_off();
					delay_ms(100);
					beep_delay(3,100);
					reset_timer_task();
					goto MPCM_send_1byte_OK;

				case MPCM_WRITE_PIR_CNT:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>PIR_SAMPLING_CNT_MAX){
						convert.i=PIR_SAMPLING_CNT_MAX;
					}
					if(convert.i<PIR_SAMPLING_CNT_MIN){
						convert.i=PIR_SAMPLING_CNT_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_byte(&eep_pir_onboard_cnt,convert.b[0]);
					eeprom_busy_wait();
					if(convert.b[0]!=eeprom_read_byte(&eep_pir_onboard_cnt)){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_MMA_X:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>MMA_OFS_MAX){
						convert.i=MMA_OFS_MAX;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[0],convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_mma_learn[0])){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_MMA_Y:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>MMA_OFS_MAX){
						convert.i=MMA_OFS_MAX;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[1],convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_mma_learn[1])){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_MMA_Z:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>MMA_OFS_MAX){
						convert.i=MMA_OFS_MAX;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_mma_learn[2],convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_mma_learn[2])){
						data_out[3]=MPCM_NOT_OK;
						reset_timer_task();
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_LM35_O1:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>LM35_OFS_MAX){
						convert.i=LM35_OFS_MAX;
					}
					if(convert.i<LM35_OFS_MIN){
						convert.i=LM35_OFS_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_learn_o1,convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_lm35_learn_o1)){
						data_out[3]=MPCM_NOT_OK;
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_LM35_O2:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>LM35_OFS_MAX){
						convert.i=LM35_OFS_MAX;
					}
					if(convert.i<LM35_OFS_MIN){
						convert.i=LM35_OFS_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_word(&eep_lm35_learn_o2,convert.i);
					eeprom_busy_wait();
					if(convert.i!=eeprom_read_word(&eep_lm35_learn_o2)){
						data_out[3]=MPCM_NOT_OK;
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_OFS_PIR_CNT:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>PIR_OFS_MAX){
						convert.i=PIR_OFS_MAX;
					}
					eeprom_busy_wait();
					eeprom_update_byte(&eep_pir_onboard_learn_cnt,convert.b[0]);
					eeprom_busy_wait();
					if(convert.b[0]!=eeprom_read_byte(&eep_pir_onboard_learn_cnt)){
						data_out[3]=MPCM_NOT_OK;
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				case MPCM_WRITE_MMA_SAMPLING:
					convert.b[1]=data_in[3];
					convert.b[0]=data_in[4];
					if(convert.i>MMA_SAMPLING_CNT_MAX){
						convert.i=MMA_SAMPLING_CNT_MAX;
					}
					if(convert.i<MMA_SAMPLING_CNT_MIN){
						convert.i=MMA_SAMPLING_CNT_MIN;
					}
					eeprom_busy_wait();
					eeprom_update_byte(&eep_mma_sampling_setpoint,convert.b[0]);
					eeprom_busy_wait();
					if(convert.b[0]!=eeprom_read_byte(&eep_mma_sampling_setpoint)){
						data_out[3]=MPCM_NOT_OK;
						goto MPCM_send_1byte;
					}
					goto MPCM_reset_and_send_1byte_OK;

				default:
					return;// Do nothing
				break;
			}
			
			MPCM_reset_and_send_1byte_OK:
				init_value_eeprom();
				beep_off();
				delay_ms(100);
				beep_delay(3,100);
				reset_timer_task();
			MPCM_send_1byte_OK:
				data_out[3]=MPCM_OK; 			/* Acknowledge! */
			MPCM_send_1byte:
				// Resp stat for master
				data_out[0]=THIS_MODULE_ID_ADDR;
				data_out[1]=3;					// Length data
				data_out[2]=data_in[MPCM_INS_CMD_OFS];
				data_out[4]=std_crc_calc(data_out,4,CRC_MPCM_POLY);
				mpcm_send(&data_out[0],5);
				return;
			MPCM_send_2byte:
				data_out[0]=THIS_MODULE_ID_ADDR;
				data_out[1]=4;					// Length data
				data_out[2]=data_in[MPCM_INS_CMD_OFS];
				data_out[5]=std_crc_calc(data_out,5,CRC_MPCM_POLY);
				mpcm_send(&data_out[0],6);
		}
	}
}
