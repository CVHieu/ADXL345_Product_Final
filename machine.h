/* ---------------------------------------------------------------------------
	Machine config header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#ifndef _MACHINE_H_
#define _MACHINE_H_


/* ------------------------- All include file ------------------------- */
// AVR core header file include
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include <avr/wdt.h>

// Module header file included
#include "io_define.h"
#include "gpio.h"
#include "std_crc.h"
#include "adc.h"
#include "usart.h"
#include "lm35.h"
#include "timer.h"
#include "eint.h"
#include "mpcm.h"
#include "debug.h"
#include "proj_eep.h"
#include "spi.h"
#include "mma7455.h"

/* ------------------------- All definitions -------------------------*/
/* Module ID*/
#define THIS_MODULE_ID_ADDR		0x01

#define DISABLE 0
#define ENABLE	1

/*---------- Sampling setpoints ----------*/
//LIMITSW
// v6: bug found & fixed, ++sampling time to 3s in 10ms sampling period (to sync w/ main backup sampling)
// #define LIMITSW_SAMPLING_TIME	10		// 1s, sampling period: 100ms 
#define LIMITSW_SAMPLING_TIME	300		// 1s, sampling period: 100ms 

// SMOKE
#define SMOKE_SAMPLING_TIME		1500	// 15s, sampling period: 10ms

//PIR
#define PIR_EN_TIME				60		// tgian on dinh (giay)
#define PIR_SAMPLING_TIME		4		// thoi gian chong nhieu
#if _PROJ_DEBUG_
#define PIR_SAMPLING_CNT_TIME	100		// thoi gian cho phep trong qua trinh phat hien
#else
#define PIR_SAMPLING_CNT_TIME	7		// thoi gian cho phep trong qua trinh phat hien
#endif
#define PIR_SAMPLING_CNT		7		// so lan phat hien
#define LEARNING_PIR_CNT		0		// tham so mac dinh learning pir cnt
#define PIR_OFS_MAX				4		// offset lon nhat cho phep cai dat
#define PIR_SAMPLING_CNT_MAX	10		// so lan phat hien lon nhat cho phep cai dat
#define PIR_SAMPLING_CNT_MIN	5		// so lan phat hien nho nhat cho phep cai dat

//MMA
#define MMA_SAMPLING_CNT_DEFAULT	3
#define MMA_SAMPLING_CNT_MIN	1
#define MMA_SAMPLING_CNT_MAX	5
#define MMA_SAMPLING_CNT_TIME	20		// Timeout for sampling MMA
#if _PROJ_DEBUG_
#define MMA_SAMPLING_DELTA		1000	//Test
#else
#define MMA_SAMPLING_DELTA		20		//(N/A) << Calibrated!
#endif
#define MMA_LEANING				0
#define MMA_OFS_MAX				20		// offset lon nhat cho phep cai dat
#define MMA_DELTA_MIN			5		// khoang delta nho nhat cho phep cai dat
#define MMA_DELTA_MAX			30		// khoang delta lon nhat cho phep cai dat

// v6: shake
#define SHAKE_SAMPLING_DELTA					600 // TODO: calib required
#define SHAKE_SAMPLING_DELTA_CNT_MAX	3 // TODO: calib required, usal greater than MMA (3)

// LM35
#define LM35_O1_SAMPLING_DELTA	20		// nhiet do delta LM35 onboard
#define LM35_O1_SAMPLING_SP_ERR	100
#define LM35_O2_SAMPLING_DELTA	20		// nhiet do delta LM35 extern
#define LM35_O2_SAMPLING_SP_ERR	100
#define LM35_LEARNING			30		// nhiet do mac dinh ban dau cua LM35
#define LM35_OFS_MIN			20		// offset lon nhat cho phep cai dat
#define LM35_OFS_MAX			50		// offset lon nhat cho phep cai dat
#define LM35_DELTA_MIN			10		// khoang delta nho nhat cho phep cai dat
#define LM35_DELTA_MAX			50		// khoang delta lon nhat cho phep cai dat

// SMOKE
#define SMOKE_TIME_ADD			60		// (1 phut) thoi gian de cong them gia tri vao gtri ban dau Smoke
#define SMOKE_SAMPLING_ADD		2		// gia tri cong them vao gia tri begin
#define SMOKE_ALARM_TIMEOUT		5		// thoi gian lien tuc co su vuot nguong khoi
#define SMOKE_SAMPLING_SP		200		// gia tri delta smoke mac dinh
#define TEMP_TIME_ADD			300		// (5 phut) thoi gian de cong them gia tri vao gtri ban dau Smoke
#define TEMP_SAMPLING_ADD		1		// gia tri temp cong them vao gia tri begin
#define TEMP_SAMPLING_O_SP		3		// do tang nhiet do onboard bo tro cho Smoke
#define TEMP_SAMPLING_E_SP		4		// do tang nhiet do extern bo tro cho Smoke

// USART
#define DISABLE					0
#define ENABLE					1

/* Sensor Type defines */
#define SENSOR_TYPE_ONOFF		0
#define SENSOR_TYPE_ADC			1
#define SENSOR_TYPE_MMA			2
#define SENSOR_TYPE_SMOKE		3
#define LEARNING_MMA			4
#define LEARNING_LM35			5
#define LEARNING_PIR			6
// v6: shake
#define SENSOR_TYPE_SHAKE		7



/*---------- System ----------*/
// Running mode:
#define SYS_RUNMODE_NORMAL 		0
#define SYS_RUNMODE_CONFIG 		1
#define SYS_RUNMODE_TEST		2

/*---------- Warning - Alarm ----------*/
#define WARNING_TIME_DIS		5		// Thoi gian giua cac lan warning
#define WARNING_BUZZ_CNT		6		// So lan bao coi buzz (chia 2)

union con_byte{
	uint16_t i;
	uint8_t b[2];
};

/*---------- Structure manipulates sensor sampling ----------*/
struct OnOffSensorSampling_Type {
	volatile uint8_t en;
	uint8_t alarm;
	volatile uint16_t cnt;
	uint8_t samplingsp;
	uint8_t cntsp;
};

struct ADCSensorSampling_Type {
	uint8_t finish;
	uint8_t buff[9];
	uint8_t cnt;
	uint8_t data;
	uint16_t samplingsp;
	// v6:
	uint16_t smoke_pir_crosscheck_sp;
};

struct _mma_flag_bf{
	uint8_t finish:1;
	uint8_t begin:1;
	uint8_t error:1;
};

union _mma_sampling_flag{
	struct _mma_flag_bf bf;
	uint8_t all;
};

struct MMASampling_Type {
	union _mma_sampling_flag flag;
	uint32_t dem;
	uint16_t buff_x[9];
	uint16_t buff_y[9];
	uint16_t buff_z[9];
	uint8_t cnt;
	uint16_t data_x;
	uint16_t data_x_buff;
	uint16_t data_y;
	uint16_t data_y_buff;
	uint16_t data_z;
	uint16_t data_z_buff;
	uint16_t data_x_min;
	uint16_t data_x_max;
	uint16_t data_y_min;
	uint16_t data_y_max;
	uint16_t data_z_min;
	uint16_t data_z_max;
	uint16_t sampling_delta[3];
	uint8_t	samplingsp;
	uint8_t	sampling_setpoint;
};

struct Learning_mma_Type{
	uint16_t data_x;
	uint16_t data_y;
	uint16_t data_z;
};

struct Learning_lm35_Type{
	uint8_t	finish;
	uint8_t cnt;
	uint8_t buff[10];
	uint8_t result;
};

struct Learning_pir_Type{
	volatile uint8_t flag;
	volatile uint8_t time;
	volatile uint8_t cnt;
	volatile uint8_t result;
};

/*
// v6: for MMA cross-checking w/ shake
struct ShakeSensorSampling_Type {
	union _mma_sampling_flag flag; // share w/ MMA type
	uint16_t buff[9];
	uint8_t cnt;
	uint16_t data_buff;
	uint16_t data;
	uint16_t data_min;
	uint16_t data_max;
	uint16_t deltaSP; 
	uint8_t deltaSPCnt;
	uint8_t deltaSPCntMax;
	uint8_t alarm; // internal use within one loop (100ms)!
};
*/

/* ----------------------------------------------------------------------------
* Extern functions and variables
* ---------------------------------------------------------------------------- */
extern union con_byte convert;

// All sensors
extern struct OnOffSensorSampling_Type pir_o;
extern struct OnOffSensorSampling_Type limitsw;
extern struct OnOffSensorSampling_Type smoke;
extern struct ADCSensorSampling_Type lm35_o1;
extern struct ADCSensorSampling_Type lm35_o2;
extern struct MMASampling_Type mma;
extern struct Learning_mma_Type learning_mma;
extern struct Learning_lm35_Type learning_lm35_o1;
extern struct Learning_lm35_Type learning_lm35_o2;
extern struct Learning_pir_Type learning_pir_o;

// Global sensor states
extern volatile uint8_t allSensorsStat;
extern volatile uint8_t module_alarm_state;
extern volatile uint8_t sys_runmode,rs_uart_cnt;
extern volatile uint8_t module_alarm_bitmask;
extern volatile uint8_t module_warning_bitmask;

/*---------- Functions  ----------*/
// Public from main.c

void reset_module_alarm_state(void);
void reset_sensor_sampling(void *ss, uint8_t ssType);

#endif
