/* ---------------------------------------------------------------------------
	GPIO config source file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
--------------------------------------------------------------------------- */
#include "machine.h"

void delay_ms(uint16_t time){
	if(time>100){
		while(time>100){
			_delay_ms(100);
			time-=100;
			wdt_reset();
		}
		_delay_ms(time);
		wdt_reset();
	}
	else{
		_delay_ms(time);
		wdt_reset();
	}
}


void beep (unsigned char times){
unsigned char i;
	for (i=0;i<times;i++){
		beep_on()
		mma.flag.bf.error=1;
		delay_ms(50);
		beep_off()
		mma.flag.bf.error=0;
		delay_ms(50);
	}
	wdt_reset();
}


void beep_delay (unsigned char loop,unsigned int time){
unsigned char i;
	for (i=0;i<loop;i++){
		beep_on()
		mma.flag.bf.error=1;
		delay_ms(time);
		beep_off()
		delay_ms(50);
		mma.flag.bf.error=0;
		delay_ms(time);
	}
}

void beep_long(void)
{
	mma.flag.bf.error=1;
	beep_on();
}
