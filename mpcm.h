/* ---------------------------------------------------------------------------
	MPCM instruction set header file
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _MPCM_H_
#define _MPCM_H_

#include "machine.h"

extern volatile uint8_t flag_en_smoke, ac_detect, en_smoke;
extern volatile uint8_t module_error_state;


extern void beep_delay(unsigned char loop,unsigned int time);
extern void delay_ms(uint16_t time);
extern void reset_smoke(void);
extern void reset_timer_task(void);

#define CRC_MPCM_POLY		0x07

/* ------------------------------------------------------------------
 * MPCM protocol
 * Request from Master:
 * SLAVE ADDRESS | LEN | DAT[0] | ... | DAT[LEN-2] | CRC
 * - DAT[0]: CMD
 * - DAT[1..LEN-2]: ARG
 * Response from Slave:
 * SLAVE ADDRESS | LEN | DAT[0] | ... | DAT[LEN-2] | CRC
 * - DAT[0]: CMD (requested by master)
 * - DAT[1..LEN-2]: value return by slave
 * ------------------------------------------------------------------ */
 
 
 /* ----------------------------------------------------------
  * INSTRUCTION
  * ---------------------------------------------------------- */
  
/* -------------------- General ---------------------------- */
// All following ins. will return 1bytes for resp. result
#define MPCM_PING					0xFF	// 'PING' cmd
#define MPCM_RUNMODE_NORMAL			0xFE	// Enter normal state
#define MPCM_RUNMODE_CONFIG			0xFD	// Enter configure state
#define MPCM_AC_ON					0xFC	// AC_on
#define MPCM_AC_OFF					0xFB	// AC_off
#define MPCM_CLR_ALARM				0xFA	// Clear alarm status
#define MPCM_WRITE_ALARM_BITMASK	0xF9
#define MPCM_READ_ALARM_BITMASK		0xF8
#define MPCM_RESTART_SENSOR			0xF7	// Restart sensor by WDT
#define MPCM_RUNMODE_TEST			0xF6	// Enter test state
#define MPCM_FACTORY_RESET			0xF5



/* ----------------- Set value ------------------------ */
// Read operation
#define MPCM_READ_MODULE_STAT 		0x01
#define MPCM_READ_MMA_DELTA			0x02
#define MPCM_READ_LM35_ON_1_DELTA	0x03
#define MPCM_READ_LM35_ON_2_DELTA	0x04
#define MPCM_READ_SMOKE_DELTA		0x05
#define MPCM_READ_PIR_TIME			0x06
#define MPCM_READ_PIR_CNT			0x07
#define MPCM_READ_LEARN_MMA_X   	0x08       	// Read mma learn X value
#define MPCM_READ_LEARN_MMA_Y   	0x09       	// Read mma learn Y value
#define MPCM_READ_LEARN_MMA_Z   	0x0A      	// Read mma learn Z value
#define MPCM_READ_OFS_MMA_X     	0x0B      	// Read mma offset X value
#define MPCM_READ_OFS_MMA_Y     	0x0C      	// Read mma offset Y value
#define MPCM_READ_OFS_MMA_Z     	0x0D      	// Read mma offset Z value
#define MPCM_READ_LEARN_LM35_O1  	0x0E
#define MPCM_READ_OFS_LM35_O1  		0x0F
#define MPCM_READ_LEARN_LM35_O2  	0x10
#define MPCM_READ_OFS_LM35_O2  		0x11
#define MPCM_READ_LEARN_PIR_CNT		0x12
#define MPCM_READ_OFS_PIR_CNT		0x13
#define MPCM_READ_MMA_SAMPLING		0x14		// Update 30/07/2013


// Write operation
#define MPCM_WRITE_MMA_DELTA		0x62
#define MPCM_WRITE_LM35_ON_1_DELTA	0x63
#define MPCM_WRITE_LM35_ON_2_DELTA	0x64
#define MPCM_WRITE_SMOKE_DELTA		0x65
#define MPCM_WRITE_PIR_TIME			0x66
#define MPCM_WRITE_PIR_CNT			0x67
#define MPCM_WRITE_OFS_MMA_X   		0x68       	// Set MMA offset X value
#define MPCM_WRITE_OFS_MMA_Y   		0x69       	// Set MMA offset Y value
#define MPCM_WRITE_OFS_MMA_Z   		0x6A      	// Set MMA offset Z value
#define MPCM_WRITE_OFS_LM35_O1 		0x6B
#define MPCM_WRITE_OFS_LM35_O2   	0x6C
#define MPCM_WRITE_OFS_PIR_CNT		0x6D
#define MPCM_WRITE_MMA_SAMPLING		0x6E		// Update 30/07/2013


/* --------------------------------------------------
 * INSTRUCTION IMPLEMENTATION
 * -------------------------------------------------- */
// Define offset of each element in ins.
#define MPCM_INS_ADDR_OFS		0 	// Address offset
#define MPCM_INS_LEN_OFS		1	// Len offset
#define MPCM_INS_CMD_OFS		2	// CMD offset


// ------------- Ins. 'MPCM_READ_MODULE_STAT' --------------------- */
#define MPCM_BITSTAT_MMA		0	// MMA
#define MPCM_BITSTAT_LIMITSW	1	// MECHANICAL SWITCH / THERMAL RELAY
#define MPCM_BITSTAT_SHAKE		2	// SHAKE sensor
#define MPCM_BITSTAT_PIR_O		3	// PIR ONBOARD sensor
//#define MPCM_BITSTAT_PIR_E		4	// PIR EXTERN sensor
#define MPCM_BITSTAT_LM35_O1	5 	// LM35 for temp. on module 1
#define MPCM_BITSTAT_LM35_O2	6 	// LM35 for temp. on module 2
#define MPCM_BITSTAT_SMOKE		7 	// Smoke on module


#define MPCM_NOT_OK				0		// return NOT_OK for MPCM
#define MPCM_OK					1		// return OK for MPCM

#define AC_ON					0
#define AC_OFF					1

// Bit su dung trong ping tra ve trang thai
#define MPCM_PING_MODE_MASK		0xF0
#define MODE_MASK				4
#define AC_MASK					5		// bit thu 5
#define ALARM_MASK				6		// bit thu 6
#define TEST_MASK				7


#define GOOD	1
#define ERROR	0

// MODULE ALARM BITMASK
/* Full states

#define MPCM_ALARM_BITMASK	(_BV(MPCM_BITSTAT_MMA)|_BV(MPCM_BITSTAT_LIMITSW)|\
							_BV(MPCM_BITSTAT_SHAKE)|_BV(MPCM_BITSTAT_PIR_O)|_BV(MPCM_BITSTAT_PIR_E)|\
							_BV(MPCM_BITSTAT_LM35_O)|_BV(MPCM_BITSTAT_LM35_E)|_BV(MPCM_BITSTAT_SMOKE))
*/

#define MPCM_ALARM_BITMASK	(_BV(MPCM_BITSTAT_MMA)|_BV(MPCM_BITSTAT_LIMITSW)|\
							_BV(MPCM_BITSTAT_PIR_O)|_BV(MPCM_BITSTAT_LM35_O1)|_BV(MPCM_BITSTAT_LM35_O2)|\
							_BV(MPCM_BITSTAT_SMOKE))

#define MPCM_ALL_BITMASK		(_BV(MPCM_BITSTAT_MMA)|_BV(MPCM_BITSTAT_LIMITSW)|\
							_BV(MPCM_BITSTAT_PIR_O)|_BV(MPCM_BITSTAT_LM35_O1)|_BV(MPCM_BITSTAT_LM35_O2)|\
							_BV(MPCM_BITSTAT_SMOKE))

#define MPCM_WARNING_BITMASK		0
								
void MPCM_Process_Data(void);
void MPCM_Response(void);
void MPCM_Init(void);
void MPCM_release(void);

extern void init_value_eeprom(void);


#endif
