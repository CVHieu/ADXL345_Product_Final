/* ---------------------------------------------------------------------------
	Header file for Timer
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#ifndef _TIMER_H_
#define _TIMER_H_

// Counter for task timer
volatile uint8_t timer_task_10ms,timer_task_100ms;
volatile uint8_t task_10ms_flag,task_100ms_flag,task_1s_flag;
extern volatile uint8_t module_error_state,mpcm_phase;

// Timer info.
// timer 1: tasks timer

void timer1_init(void);
void simple_tasks_process(void);
void reset_timer_task(void);


// extern user tasks function (need to be referenced!)
extern void user_task_10ms(void);
extern void user_task_100ms(void);
extern void user_task_1s(void);

#endif
