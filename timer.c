/* ---------------------------------------------------------------------------
	Source file for Timer
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
 --------------------------------------------------------------------------- */
#include "machine.h"

/* ---------------------------------------------------------------------------
	Timer for tasks (timer 1)
 	Interrupt used! 100Hz ~ 10ms
--------------------------------------------------------------------------- */

void timer1_init(void){
	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 250.000 kHz
	// Mode: CTC top=OCR1A
	// Timer 1 Overflow Interrupt: Off
	// Compare A Match Interrupt: On
	TCCR1A=0x00;
	TCCR1B=(_BV(WGM12)|_BV(CS11)|_BV(CS10));
	// OCR1A  = 2500
	OCR1AH=0x09;
	OCR1AL=0xC4;
	
	// Timer(s)/Counter(s) Interrupt(s) initialization
	TIMSK=_BV(OCIE1A);	// Interrupt when TCNT1 = OCR1A

	timer_task_10ms=0;
	timer_task_100ms=0;
	timeout_usart=0;
	task_10ms_flag=0;
	task_100ms_flag=0;
	task_1s_flag=0;
}


/* ---------------------------------------------------------------------------
	Task executive due to task timer
--------------------------------------------------------------------------- */

void simple_tasks_process(void){
	// Executive task 10ms period
	if (task_10ms_flag){
		user_task_10ms();
		task_10ms_flag=0;
	}
	
	// Executive task 100ms period
	if (task_100ms_flag){
		user_task_100ms();
		task_100ms_flag=0;
	}

	// Executive task 1s period
	if (task_1s_flag){
		user_task_1s();
		task_1s_flag=0;
	}
}


/* ---------------------------------------------------------------------------
	Interrupt sub-routine reference section 
 --------------------------------------------------------------------------- */
// Compare A timer 1 interrupt
ISR(TIMER1_COMPA_vect){
	// 10ms++
	timer_task_10ms++;
	task_10ms_flag=1;
	if (timer_task_10ms==10){
		timer_task_10ms=0;
		// 100ms++
		timer_task_100ms++;
		task_100ms_flag=1;
		if (timer_task_100ms==10){
			timer_task_100ms=0;
			// 1s++
			task_1s_flag=1;
		}
	}
}

void reset_timer_task (void){
	timer_task_10ms=0;
	timer_task_100ms=0;
	task_10ms_flag=0;
	task_100ms_flag=0;
	task_1s_flag=0;
}
