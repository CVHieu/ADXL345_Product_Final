/* ---------------------------------------------------------------------------
	Header file for MMA7455
	MCU ATmega16	XT 16MHz
	FIS project Sensor Board
	binhdtt2@fpt.com.vn
	30/10/2012
 	Firmware v5.0  Hardware v5.0
---------------------------------------------------------------------------*/
#ifndef _MMA_7260_H_
#define _MMA_7260_H_

#define MMA7455_I2C_ADDRESS      0x1D
#define WRITE	0x00
#define READ	0x80

//MMA7455 Registers
#define MMA7455_X_OUTL				0x32
#define MMA7455_X_OUTH				0x33
#define MMA7455_Y_OUTL				0x34
#define MMA7455_Y_OUTH				0x35
#define MMA7455_Z_OUTL				0x36
#define MMA7455_Z_OUTH				0x37
//#define MMA7455_X_OUT8				0x06	//8 bit register containing value for X
//#define MMA7455_Y_OUT8				0x07	//8 bit register containing value for Y
//#define MMA7455_Z_OUT8				0x08	//8 bit register containing value for Z
//#define MMA7455_STATUS				0x09
//#define MMA7455_DETECT_SOURCE		0x0A	//Detection source register
//#define MMA7455_TOUT				0x0B
//#define MMA7455_I2CAD				0x0D
//#define MMA7455_USRINF				0x0E
#define MMA7455_WHOAMI				0x00
//#define MMA7455_XOFFL				0x10
//#define MMA7455_XOFFH				0x11
//#define MMA7455_YOFFL				0x12
//#define MMA7455_YOFFH				0x13
//#define MMA7455_ZOFFL				0x14
//#define MMA7455_ZOFFH				0x15
//#define MMA7455_MODE_CONTROL		0x16	//Mode control register
//#define MMA7455_INTRST				0x17
//#define MMA7455_CONTROL1			0x18
//#define MMA7455_CONTROL2			0x19
//#define MMA7455_LEVELDETECT			0x1A
//#define MMA7455_PULSE_DET			0x1B	//Pulse detection threshold limit value
//#define MMA7455_PW					0x1C
//#define MMA7455_LT					0x1D
//#define MMA7455_TW					0x1E

// ---------- Control register value ----------
// Sensitivity
#define MMA7455_2G		0x00	//Set Sensitivity to 2g
#define MMA7455_4G		0x09	//Set Sensitivity to 4g
#define MMA7455_8G		0x0A	//Set Sensitivity to 8g
// Run mode
#define MMA7455_STANDBY			0x00	// Mode tam dung
#define MMA7455_MEASURE			0x08	// Mode do gia toc
//#define MMA7455_LEVEL_DETECT	0x02
//#define MMA7455_PULSE_DETECT	0x03
// SPI 3W mode
#define MMA7455_SPI_3W			0x20
#define MMA7455_SPI_4W			0x00

#define ADXL345_XOFF				0x1E
#define ADXL345_YOFF				0x1F
#define ADXL345_ZOFF				0x20
#define _POWER_CTL     			    0x2D
#define _DATA_FORMAT    			0x31
#define _BW_RATE                    0x2C
#define _SPEED                      0x0F 

//VALUES
//#define Z_PULSE						0x4  //Pulse detected on Z-axis
//#define SENSITIVITY_DEFAULT			0x25 //Default sensitivity level

void init_MMA7455(void);
void MMA7455_write_byte(uint8_t add,uint8_t data);
uint8_t MMA7455_read_byte(uint8_t add);
void read_MMA7455(uint16_t *x, uint16_t *y, uint16_t *z);

#endif
